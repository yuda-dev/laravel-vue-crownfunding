-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 26, 2020 at 04:31 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crownfunding`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
('0d252f61-777d-4aa7-b19b-9e4fa550840a', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/0d252f61-777d-4aa7-b19b-9e4fa550840a.jpg', '2020-11-28 23:05:27', '2020-11-28 23:05:27'),
('326560af-a19c-429b-9351-2fd1ac99323e', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/326560af-a19c-429b-9351-2fd1ac99323e.jpg', '2020-11-28 23:04:54', '2020-11-28 23:04:54'),
('5b3c3184-12cf-44a0-8f13-8fa6fe919889', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/5b3c3184-12cf-44a0-8f13-8fa6fe919889.jpg', '2020-11-28 23:04:29', '2020-11-28 23:04:29'),
('684d430a-f82e-4f1c-83af-c460045cb206', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/684d430a-f82e-4f1c-83af-c460045cb206.jpeg', '2020-11-28 23:05:52', '2020-11-28 23:05:52'),
('69d4c72c-2dd8-4cb3-9fc3-e8262724ca4e', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/69d4c72c-2dd8-4cb3-9fc3-e8262724ca4e.jpeg', '2020-11-28 23:05:43', '2020-11-28 23:05:43'),
('6eae1dd3-f9e1-4955-bd90-ee67c734ee6a', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/6eae1dd3-f9e1-4955-bd90-ee67c734ee6a.jpg', '2020-11-28 23:04:38', '2020-11-28 23:04:38'),
('795b4a91-4acb-4cd8-85bc-3e4d41627ecc', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/795b4a91-4acb-4cd8-85bc-3e4d41627ecc.jpg', '2020-11-28 23:05:35', '2020-11-28 23:05:35'),
('8014dbb0-b66f-43c7-8910-83ef24b5a103', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/8014dbb0-b66f-43c7-8910-83ef24b5a103.jpg', '2020-11-28 23:06:47', '2020-11-28 23:06:47'),
('a9fcc4ef-44d6-47df-a0d7-fb6114909d5a', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/a9fcc4ef-44d6-47df-a0d7-fb6114909d5a.jpeg', '2020-11-28 23:06:09', '2020-11-28 23:06:09'),
('b64f54fb-e87a-4a84-9267-dde879025167', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/b64f54fb-e87a-4a84-9267-dde879025167.jpg', '2020-11-28 23:06:28', '2020-11-28 23:06:28'),
('bedf39ae-500d-4084-ac77-37fa16523cb1', 'Bangun Pesantren Penghafal Quran di Purwakarta', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/bedf39ae-500d-4084-ac77-37fa16523cb1.jpg', '2020-11-28 23:03:57', '2020-11-28 23:03:57'),
('da6b2b3c-a6d7-4b45-9dd1-fd1acf87630b', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/da6b2b3c-a6d7-4b45-9dd1-fd1acf87630b.jpg', '2020-11-28 23:04:46', '2020-11-28 23:04:46'),
('dc301c26-182b-4441-b1d4-8acc47825793', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/dc301c26-182b-4441-b1d4-8acc47825793.jpg', '2020-11-28 23:06:54', '2020-11-28 23:06:54'),
('e8977000-21e9-431b-9385-f480e90c224a', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/e8977000-21e9-431b-9385-f480e90c224a.jpg', '2020-11-28 23:06:21', '2020-11-28 23:06:21'),
('f9bcc49f-13b2-418f-8a3f-2c785692d190', 'lorem ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/blog/f9bcc49f-13b2-418f-8a3f-2c785692d190.jpg', '2020-11-28 23:06:01', '2020-11-28 23:06:01');

-- --------------------------------------------------------

--
-- Table structure for table `campaigns`
--

CREATE TABLE `campaigns` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `required` int(11) DEFAULT NULL,
  `collected` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `campaigns`
--

INSERT INTO `campaigns` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`, `address`, `required`, `collected`) VALUES
('0807c831-8718-409d-a791-687a2612c742', 'Rumah Sehat Terpadu Dompet Dhuafa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/0807c831-8718-409d-a791-687a2612c742.jpeg', '2020-11-28 22:10:47', '2020-11-28 22:10:47', 'Indonesia', 5000000, 500000),
('28d35d5a-4a3c-47b3-85c6-90521425d273', 'Air Mata Bu Ecu Berpisah dengan Anak Karena Kanker', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/28d35d5a-4a3c-47b3-85c6-90521425d273.jpg', '2020-11-28 22:18:09', '2020-11-28 22:18:09', 'Indonesia', 5000000, 4000000),
('5ca5961b-d973-4ae1-810e-27dee9fc5d7f', 'Miris! Lansia Berjualan dengan Tangan Membusuk', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/5ca5961b-d973-4ae1-810e-27dee9fc5d7f.jpg', '2020-11-28 15:39:01', '2020-11-28 15:39:02', 'Indonesia', 5000000, 0),
('76b3ffe4-db59-4f9a-b3f7-dbf96fc35b9e', 'Ladang Jariyah: Bantu 400 Yatim Lanjut Sekolah!', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/76b3ffe4-db59-4f9a-b3f7-dbf96fc35b9e.jpeg', '2020-11-28 16:13:54', '2020-11-28 16:13:56', 'Indonesia', 5000000, 200000),
('7cb61219-796d-4c7d-9e24-bdbb63196f5e', 'Berbagi untuk Dhuafa', '“Bukanlah menghadapkan wajahmu ke arah timur dan barat itu suatu kebajikan, akan tetapi sesungguhnya kebajikan itu ialah beriman kepada Allah, hari kemudian, malaikat-malaikat, kitab-kitab, nabi-nabi dan memberikan harta yang dicintainya kepada kerabatnya, anak-anak yatim, orang-orang miskin, musafir (yang memerlukan pertolongan) dan orang-orang yang meminta-minta; dan (memerdekakan) hamba sahaya,” (Al-Baqarah : 177).', '/photos/campaign/7cb61219-796d-4c7d-9e24-bdbb63196f5e.jpg', '2020-11-25 15:09:23', '2020-11-25 15:09:26', 'Indonesia', 5000000, 0),
('801de599-2f18-4f5a-9a31-5dd9c98a30bb', 'Gagal Ginjal, Bantu Guru Honorer Bisa Cuci Darah', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/801de599-2f18-4f5a-9a31-5dd9c98a30bb.jpeg', '2020-11-28 22:13:49', '2020-11-28 22:13:49', 'Indonesia', 5000000, 500000),
('8b68650a-3657-4db2-b347-5f996099e7ba', 'Bangun Pesantren Penghafal Quran di Purwakarta', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/8b68650a-3657-4db2-b347-5f996099e7ba.jpg', '2020-11-28 22:19:35', '2020-11-28 22:19:35', 'Indonesia', 15000000, 4000000),
('8ca9d52d-5adf-41e5-b3d7-c4a188c1b28b', '30 Tahun Berjalan Jongkok Karena Kaki Lumpuh', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/8ca9d52d-5adf-41e5-b3d7-c4a188c1b28b.jpeg', '2020-11-28 16:42:25', '2020-11-28 16:42:26', 'Indonesia', 5000000, 100000),
('991f91e4-39b0-4069-83a6-436722329ad1', 'Di Usia Senja, Kanker Paru-paru Serang Tubuhnya', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/991f91e4-39b0-4069-83a6-436722329ad1.jpg', '2020-11-28 22:12:12', '2020-11-28 22:12:13', 'Indonesia', 5000000, 500000),
('a457f2fc-8854-4976-bfc7-5090fae4349e', 'Bantuan Korban Bencana Alam', 'Tujuan Penggalangan dan ini agar dapat membantu saudara saudara kita yang tertimpa bencana alam di lampung dan sekitarnya.. \n\nBantuan sekecil apapun dapat meringankan beban mereka yang tertimpa Musibah..', '/photos/campaign/a457f2fc-8854-4976-bfc7-5090fae4349e.jpg', '2020-11-25 15:02:20', '2020-11-25 15:02:21', 'Lampung', 5000000, 0),
('af78ccbe-3f1d-4ef1-a2dc-d7df0f85a17c', 'Lumpuh Otak Tak Bisa Halangiku Jadi Hafidzah', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/af78ccbe-3f1d-4ef1-a2dc-d7df0f85a17c.jpg', '2020-11-28 22:16:05', '2020-11-28 22:16:05', 'Indonesia', 5000000, 4000000),
('b8e65771-6986-45bf-b73d-290ce67ef398', 'Tanpa Ayah, Virgi Terus Bermimpi Untuk Ibu & Nenek', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/b8e65771-6986-45bf-b73d-290ce67ef398.jpg', '2020-11-28 22:14:51', '2020-11-28 22:14:51', 'Indonesia', 5000000, 500000),
('cdd959e7-fb0e-4d7a-be4c-5eccc8b19ed4', 'Bangun Pesantren Layak untuk Santri Yatim Dhuafa', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/cdd959e7-fb0e-4d7a-be4c-5eccc8b19ed4.jpg', '2020-11-28 15:40:31', '2020-11-28 15:40:32', 'Indonesia', 5000000, 0),
('d91b70a9-f8c6-4dfc-bc22-993a0bd338b5', 'Prihatin! Sedekah Makan untuk Anak Yatim', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/d91b70a9-f8c6-4dfc-bc22-993a0bd338b5.jpg', '2020-11-28 16:22:32', '2020-11-28 16:22:34', 'Indonesia', 5000000, 300000),
('dab97d1b-a70a-4dcf-8b35-22c30852bebf', 'Bantu Dekris sembuh dari Gizi Buruk', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ac dignissim eros. Nunc porta tincidunt leo, id euismod neque. Pellentesque tellus tellus, tristique vitae dui pharetra, fermentum fermentum lectus. Nullam tincidunt orci non neque pellentesque facilisis. Pellentesque euismod consectetur eros, id feugiat sem cursus eget. Maecenas mollis a arcu ut aliquam. Mauris a varius risus. Pellentesque imperdiet rhoncus tortor sit amet sagittis. Aenean augue dui, interdum viverra lacus quis, scelerisque tincidunt tortor.', '/photos/campaign/dab97d1b-a70a-4dcf-8b35-22c30852bebf.jpg', '2020-11-28 16:25:40', '2020-11-28 16:25:41', 'Indonesia', 5000000, 100000);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_jobs`
--

INSERT INTO `failed_jobs` (`id`, `connection`, `queue`, `payload`, `exception`, `failed_at`) VALUES
(1, 'database', 'default', '{\"displayName\":\"App\\\\Listeners\\\\SendEmailOtpCode\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":8:{s:5:\\\"class\\\";s:30:\\\"App\\\\Listeners\\\\SendEmailOtpCode\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:25:\\\"App\\\\Events\\\\UserRegistered\\\":3:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";s:36:\\\"808ac30f-6c69-420d-be40-aca09eb6b2c3\\\";s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:9:\\\"condition\\\";s:8:\\\"register\\\";s:6:\\\"socket\\\";N;}}s:5:\\\"tries\\\";N;s:10:\\\"retryAfter\\\";N;s:9:\\\"timeoutAt\\\";N;s:7:\\\"timeout\\\";N;s:6:\\\"\\u0000*\\u0000job\\\";N;}\"}}', 'ErrorException: Trying to get property \'otp\' of non-object in /opt/lampp/htdocs/CrownFunding/storage/framework/views/e22795a8c1ba876fa3eb7c0f204f579fc95f60a1.php:144\nStack trace:\n#0 /opt/lampp/htdocs/CrownFunding/storage/framework/views/e22795a8c1ba876fa3eb7c0f204f579fc95f60a1.php(144): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, \'Trying to get p...\', \'/opt/lampp/htdo...\', 144, Array)\n#1 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(43): include(\'/opt/lampp/htdo...\')\n#2 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(59): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(\'/opt/lampp/htdo...\', Array)\n#3 /opt/lampp/htdocs/CrownFunding/vendor/facade/ignition/src/Views/Engines/CompilerEngine.php(36): Illuminate\\View\\Engines\\CompilerEngine->get(\'/opt/lampp/htdo...\', Array)\n#4 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/View.php(143): Facade\\Ignition\\Views\\Engines\\CompilerEngine->get(\'/opt/lampp/htdo...\', Array)\n#5 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/View.php(126): Illuminate\\View\\View->getContents()\n#6 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/View.php(91): Illuminate\\View\\View->renderContents()\n#7 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(355): Illuminate\\View\\View->render()\n#8 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(328): Illuminate\\Mail\\Mailer->renderView(\'send_email\', Array)\n#9 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(246): Illuminate\\Mail\\Mailer->addContent(Object(Illuminate\\Mail\\Message), \'send_email\', NULL, NULL, Array)\n#10 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailable.php(159): Illuminate\\Mail\\Mailer->send(\'send_email\', Array, Object(Closure))\n#11 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Support/Traits/Localizable.php(19): Illuminate\\Mail\\Mailable->Illuminate\\Mail\\{closure}()\n#12 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailable.php(160): Illuminate\\Mail\\Mailable->withLocale(NULL, Object(Closure))\n#13 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(277): Illuminate\\Mail\\Mailable->send(Object(Illuminate\\Mail\\Mailer))\n#14 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(231): Illuminate\\Mail\\Mailer->sendMailable(Object(App\\Mail\\UserRegisterMail))\n#15 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/PendingMail.php(122): Illuminate\\Mail\\Mailer->send(Object(App\\Mail\\UserRegisterMail))\n#16 /opt/lampp/htdocs/CrownFunding/app/Listeners/SendEmailOtpCode.php(37): Illuminate\\Mail\\PendingMail->send(Object(App\\Mail\\UserRegisterMail))\n#17 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Events/CallQueuedListener.php(92): App\\Listeners\\SendEmailOtpCode->handle(Object(App\\Events\\UserRegistered))\n#18 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Events\\CallQueuedListener->handle(Object(Illuminate\\Foundation\\Application))\n#19 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#20 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#21 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#22 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#23 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#24 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(130): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#25 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#26 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#27 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(Illuminate\\Events\\CallQueuedListener), false)\n#28 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(130): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#29 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#30 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#31 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Events\\CallQueuedListener))\n#32 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Jobs/Job.php(88): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#33 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(368): Illuminate\\Queue\\Jobs\\Job->fire()\n#34 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(314): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#35 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(267): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#36 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(112): Illuminate\\Queue\\Worker->runNextJob(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#37 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#38 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#39 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#40 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#41 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#42 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#43 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Command.php(134): Illuminate\\Container\\Container->call(Array)\n#44 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Command/Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#45 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#46 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(1009): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#47 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(273): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#48 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(149): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#49 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#50 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#51 /opt/lampp/htdocs/CrownFunding/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#52 {main}\n\nNext Facade\\Ignition\\Exceptions\\ViewException: Trying to get property \'otp\' of non-object (View: /opt/lampp/htdocs/CrownFunding/resources/views/send_email.blade.php) in /opt/lampp/htdocs/CrownFunding/resources/views/send_email.blade.php:144\nStack trace:\n#0 /opt/lampp/htdocs/CrownFunding/resources/views/send_email.blade.php(144): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, \'Trying to get p...\', \'/opt/lampp/htdo...\', 144, Array)\n#1 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/Engines/PhpEngine.php(43): include(\'/opt/lampp/htdo...\')\n#2 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/Engines/CompilerEngine.php(59): Illuminate\\View\\Engines\\PhpEngine->evaluatePath(\'/opt/lampp/htdo...\', Array)\n#3 /opt/lampp/htdocs/CrownFunding/vendor/facade/ignition/src/Views/Engines/CompilerEngine.php(36): Illuminate\\View\\Engines\\CompilerEngine->get(\'/opt/lampp/htdo...\', Array)\n#4 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/View.php(143): Facade\\Ignition\\Views\\Engines\\CompilerEngine->get(\'/opt/lampp/htdo...\', Array)\n#5 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/View.php(126): Illuminate\\View\\View->getContents()\n#6 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/View/View.php(91): Illuminate\\View\\View->renderContents()\n#7 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(355): Illuminate\\View\\View->render()\n#8 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(328): Illuminate\\Mail\\Mailer->renderView(\'send_email\', Array)\n#9 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(246): Illuminate\\Mail\\Mailer->addContent(Object(Illuminate\\Mail\\Message), \'send_email\', NULL, NULL, Array)\n#10 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailable.php(159): Illuminate\\Mail\\Mailer->send(\'send_email\', Array, Object(Closure))\n#11 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Support/Traits/Localizable.php(19): Illuminate\\Mail\\Mailable->Illuminate\\Mail\\{closure}()\n#12 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailable.php(160): Illuminate\\Mail\\Mailable->withLocale(NULL, Object(Closure))\n#13 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(277): Illuminate\\Mail\\Mailable->send(Object(Illuminate\\Mail\\Mailer))\n#14 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/Mailer.php(231): Illuminate\\Mail\\Mailer->sendMailable(Object(App\\Mail\\UserRegisterMail))\n#15 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Mail/PendingMail.php(122): Illuminate\\Mail\\Mailer->send(Object(App\\Mail\\UserRegisterMail))\n#16 /opt/lampp/htdocs/CrownFunding/app/Listeners/SendEmailOtpCode.php(37): Illuminate\\Mail\\PendingMail->send(Object(App\\Mail\\UserRegisterMail))\n#17 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Events/CallQueuedListener.php(92): App\\Listeners\\SendEmailOtpCode->handle(Object(App\\Events\\UserRegistered))\n#18 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Events\\CallQueuedListener->handle(Object(Illuminate\\Foundation\\Application))\n#19 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#20 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#21 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#22 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#23 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#24 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(130): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#25 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#26 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Bus/Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#27 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(Illuminate\\Events\\CallQueuedListener), false)\n#28 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(130): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#29 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Pipeline/Pipeline.php(105): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(Illuminate\\Events\\CallQueuedListener))\n#30 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#31 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Events\\CallQueuedListener))\n#32 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Jobs/Job.php(88): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#33 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(368): Illuminate\\Queue\\Jobs\\Job->fire()\n#34 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(314): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#35 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(267): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#36 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(112): Illuminate\\Queue\\Worker->runNextJob(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#37 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#38 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#39 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#40 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#41 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#42 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#43 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Command.php(134): Illuminate\\Container\\Container->call(Array)\n#44 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Command/Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#45 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#46 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(1009): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#47 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(273): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#48 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(149): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#49 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#50 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#51 /opt/lampp/htdocs/CrownFunding/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#52 {main}', '2020-11-28 23:15:22'),
(2, 'database', 'default', '{\"displayName\":\"App\\\\Listeners\\\\SendEmailOtpCode\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"Illuminate\\\\Events\\\\CallQueuedListener\",\"command\":\"O:36:\\\"Illuminate\\\\Events\\\\CallQueuedListener\\\":8:{s:5:\\\"class\\\";s:30:\\\"App\\\\Listeners\\\\SendEmailOtpCode\\\";s:6:\\\"method\\\";s:6:\\\"handle\\\";s:4:\\\"data\\\";a:1:{i:0;O:25:\\\"App\\\\Events\\\\UserRegistered\\\":3:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:8:\\\"App\\\\User\\\";s:2:\\\"id\\\";s:36:\\\"44c09548-f001-4d01-b11e-ef963ce85d9a\\\";s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:9:\\\"condition\\\";s:8:\\\"register\\\";s:6:\\\"socket\\\";N;}}s:5:\\\"tries\\\";N;s:10:\\\"retryAfter\\\";N;s:9:\\\"timeoutAt\\\";N;s:7:\\\"timeout\\\";N;s:6:\\\"\\u0000*\\u0000job\\\";N;}\"}}', 'Illuminate\\Database\\Eloquent\\ModelNotFoundException: No query results for model [App\\User]. in /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Database/Eloquent/Builder.php:479\nStack trace:\n#0 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/SerializesAndRestoresModelIdentifiers.php(102): Illuminate\\Database\\Eloquent\\Builder->firstOrFail()\n#1 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/SerializesAndRestoresModelIdentifiers.php(57): App\\Events\\UserRegistered->restoreModel(Object(Illuminate\\Contracts\\Database\\ModelIdentifier))\n#2 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/SerializesModels.php(45): App\\Events\\UserRegistered->getRestoredPropertyValue(Object(Illuminate\\Contracts\\Database\\ModelIdentifier))\n#3 [internal function]: App\\Events\\UserRegistered->__wakeup()\n#4 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/CallQueuedHandler.php(53): unserialize(\'O:36:\"Illuminat...\')\n#5 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Jobs/Job.php(88): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#6 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(368): Illuminate\\Queue\\Jobs\\Job->fire()\n#7 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(314): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#8 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Worker.php(267): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#9 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(112): Illuminate\\Queue\\Worker->runNextJob(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#10 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Queue/Console/WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#11 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(36): Illuminate\\Queue\\Console\\WorkCommand->handle()\n#12 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#13 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(93): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#14 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/BoundMethod.php(37): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#15 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Container/Container.php(590): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#16 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Command.php(134): Illuminate\\Container\\Container->call(Array)\n#17 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Command/Command.php(255): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#18 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#19 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(1009): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#20 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(273): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#21 /opt/lampp/htdocs/CrownFunding/vendor/symfony/console/Application.php(149): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#22 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Console/Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#23 /opt/lampp/htdocs/CrownFunding/vendor/laravel/framework/src/Illuminate/Foundation/Console/Kernel.php(131): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#24 /opt/lampp/htdocs/CrownFunding/artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#25 {main}', '2020-11-28 23:15:23');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2011_11_03_111420_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_08_19_000000_create_failed_jobs_table', 1),
(5, '2020_11_04_092624_create_otps_table', 2),
(6, '2020_11_19_205045_create_jobs_table', 3),
(7, '2020_11_20_190929_create_campaigns_table', 4),
(8, '2020_11_20_201145_create_blogs_table', 5),
(9, '2020_11_21_175450_add_address_to_campaigns', 6);

-- --------------------------------------------------------

--
-- Table structure for table `otps`
--

CREATE TABLE `otps` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` int(11) NOT NULL,
  `user_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid_until` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `otps`
--

INSERT INTO `otps` (`id`, `otp`, `user_id`, `valid_until`, `created_at`, `updated_at`) VALUES
('4eaa01c4-030d-4159-ac30-b1298de888f6', 338784, 'db74618b-12fd-4367-8903-d74b877d4267', '2020-11-28 21:30:09', '2020-11-28 14:25:09', '2020-11-28 14:25:09');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
('6b5998dd-0fa5-429b-906e-8f515a79c2c9', 'user', '2020-11-07 15:35:46', '2020-11-07 15:35:46'),
('af315815-49c5-47e4-b77d-0f7426ec3e75', 'admin', '2020-11-07 15:35:46', '2020-11-07 15:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo_profile` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `email_verified_at`, `password`, `photo_profile`, `remember_token`, `created_at`, `updated_at`) VALUES
('808ac30f-6c69-420d-be40-aca09eb6b2c3', '6b5998dd-0fa5-429b-906e-8f515a79c2c9', 'Yuda Muhtar', 'yuda3@yuda.com', '2020-11-26 11:07:04', '$2y$10$ZCViIxk5jb1YsfTCyh5/ruy003tp6kLnYBoWmyPhZD/gA1r7AcI..', '/photos/users/photo-profile/2020-11-26 20:56:32-hoenix-kind-logo-vector-phoenix-bird-vector-11562899990gcdoe5osxb.png', NULL, '2020-11-26 11:04:14', '2020-11-26 13:56:32'),
('91e3b1ba-35d4-49e3-af91-ebc0ff912837', '6b5998dd-0fa5-429b-906e-8f515a79c2c9', 'Yuda Muhtar', 'yudainformatika@gmail.com', '2020-11-28 23:24:40', NULL, NULL, NULL, '2020-11-28 23:23:52', '2020-11-28 23:24:40'),
('db74618b-12fd-4367-8903-d74b877d4267', '6b5998dd-0fa5-429b-906e-8f515a79c2c9', 'Yuda Muhtar', 'yudainformatika125@gmail.com', NULL, '$2y$10$jfTyGFmqEUTMkFp3wCmd1ef3LJ978U0gAL0klSpDQjJ4tAl5IwD0u', 'https://lh3.googleusercontent.com/a-/AOh14GjI1Pf_ma84QpRKyiLAmJcnGMx2Gi39EeAUrQ3KFg=s96-c', NULL, '2020-11-28 14:25:09', '2020-11-28 14:25:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campaigns`
--
ALTER TABLE `campaigns`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otps`
--
ALTER TABLE `otps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `otps_user_id_foreign` (`user_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `otps`
--
ALTER TABLE `otps`
  ADD CONSTRAINT `otps_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
